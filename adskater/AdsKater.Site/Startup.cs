﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AdsKater.Site.Startup))]
namespace AdsKater.Site
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
