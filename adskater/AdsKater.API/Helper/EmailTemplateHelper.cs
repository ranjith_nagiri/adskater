﻿using System;
using System.IO;
using System.Net.Mail;

namespace AdsKater.API.Helper
{
    public static class EmailTemplateHelper
    {
        //private static Logger logger = LogManager.GetCurrentClassLogger();

        public static string RegisterEmailPopulateBody(string mobileNumber, string userEmailAddress, string link)
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "aftersignup.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{MobileNumber}", mobileNumber);
            body = body.Replace("{UserEmailAddress}", userEmailAddress);
            return body;
        }

        public static string PaymentSuccessEmailPopulateBody(string propertyName)
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "paymentsuccess.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }
            //body = body.Replace("{PropertyName}", propertyName);
            return body;
        }

        public static string PopulateWinnerEmail(string propertyName, string winningPrize)
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "winneremail.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{WinningPrize}", winningPrize);
            body = body.Replace("{PropertyPurchase}", propertyName);
            return body;
        }

        public static string PopulateDrawNotProceedEmail()
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "sendemailtodrawusers.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }
            return body;
        }

        public static string PaymentFailureEmailPopulateBody(string propertyName)
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "paymentfailure.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }
            //body = body.Replace("{PropertyName}", propertyName);
            return body;
        }

        public static string ResetPasswordEmailPopulateBody(string email, string newPassword)
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "passwordreset.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{MobileNumber}", email);
            body = body.Replace("{NewPassword}", newPassword);
            return body;
        }

        public static string SendSupportMailEmailPopulateBody(string username, string comments, string email, string query)
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "supportmail.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{UserName}", username);
            body = body.Replace("{Comments}", comments);
            body = body.Replace("{UserEmail}", email);
            body = body.Replace("{QueryMessage}", query);
            return body;
        }

        public static string PaymentSuccessVendorEmailPopulateBody(string vendorName, string userName, string purchaseAmount, string vendorCountry, double convertRate)
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "paymentemailtovendor.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{VendorName}", vendorName);
            body = body.Replace("{UserName}", userName);
            body = body.Replace("{PurchagedAmount}", vendorCountry == "india" ? "Rs " + purchaseAmount : "$ " + (Convert.ToDouble(purchaseAmount) / convertRate).ToString("0.00"));
            return body;
        }

        public static string MEPaymentSuccessVendorEmailPopulateBody(string userName = "", string purchaseAmount = "")
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "paymentsuccess.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }

            return body;
        }

        public static string PaymentSuccessVendorRedeemEmailPopulateBody(string vendorName, string userName, string purchaseAmount, string vendorCountry, double convertRate)
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "redeemamountemailtovendor.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{VendorName}", vendorName);
            body = body.Replace("{UserName}", userName);
            body = body.Replace("{PurchagedAmount}", vendorCountry == "india" ? "Rs " + purchaseAmount : "$ " + (Convert.ToDouble(purchaseAmount) / convertRate).ToString("0.00"));
            return body;
        }

        public static string SendActivationLink(string name, string activationLink)
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "activationlink.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{UserName}", name != null ? name : "User");
            body = body.Replace("{UserActivationLinkAddress}", activationLink);
            return body;
        }

        public static string SendDrawApproveLink(string drawName, string approveLink)
        {
            string body = string.Empty;
            string documentpath = System.Web.HttpContext.Current.Server.MapPath("~") + "EmailTemplates\\" + "drawcreate.html";
            using (StreamReader reader = new StreamReader(documentpath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{DrawName}", drawName);
            body = body.Replace("{DrawLink}", approveLink);
            return body;
        }

        public static bool SendEmail(string toEmail, string emailBody, string subject, bool isMultiUser = false)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient clientServer = new SmtpClient();
                clientServer.Host = AppConfigurationProperty.GetConfigurationValueByKey("SmtpClient");
                clientServer.Port = Convert.ToInt32(AppConfigurationProperty.GetConfigurationValueByKey("SmtpClientPort"));

                mail.From = new MailAddress(AppConfigurationProperty.GetConfigurationValueByKey("FromEmailAddress"));
                if (isMultiUser)
                    mail.CC.Add(toEmail);
                else
                    mail.To.Add(toEmail);
                mail.Subject = subject;
                mail.Body = emailBody;//EmailTemplateHelper.SendActivationLink(name, toEmail, activationLink);
                mail.IsBodyHtml = true;
                clientServer.Credentials = new System.Net.NetworkCredential(AppConfigurationProperty.GetConfigurationValueByKey("SmtpClientUserName"), AppConfigurationProperty.GetConfigurationValueByKey("SmtpClientUserPassword"));
                clientServer.EnableSsl = true;
                //clientServer.UseDefaultCredentials = false;
                clientServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                clientServer.Timeout = 20000;
                clientServer.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                //logger.Error($"Send Email Link - {ex.Message}" + Environment.NewLine + DateTime.Now);
                return false;
            }
        }
    }
}