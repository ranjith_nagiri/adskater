﻿using System;
using System.Collections.Specialized;
using System.Net;

namespace AdsKater.API.Helper
{
    public static class TextLocalSMS
    {
        public static string SendSMS(string message, string countryCode, string mobilenumber)
        {
            using (var wb = new WebClient())
            {
                byte[] response = wb.UploadValues("https://api.textlocal.in/send/", new NameValueCollection()
                {
                {"apikey" , AppConfigurationProperty.GetConfigurationValueByKey("TextLocalSMSKey")},
                {"numbers" , $"{countryCode}{mobilenumber}"},
                {"message" , message},
                {"sender" , "WNPROP"}
                });
                string result = System.Text.Encoding.UTF8.GetString(response);
                return result;
            }
        }

        public static int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }
    }
}