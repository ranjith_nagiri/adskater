﻿using AdsKater.DbModels;
using System.Web.Configuration;

namespace AdsKater.API.Helper
{
    public static class AppConfigurationProperty
    {
        public static string GetConfigurationValueByKey(string key)
        {
            return WebConfigurationManager.AppSettings[key].ToString();
        }

        public static string GetUserEmail(AspNetUser user)
        {
            if (user != null)
            {
                if (user.UserName + AppConfigurationProperty.GetConfigurationValueByKey("EmailExtension") == user.Email)
                    return string.Empty;
                else
                    return user.Email;
            }
            else
            {
                return string.Empty;
            }
        }
    }
}