﻿using AdsKater.DbModels;
using AdsKater.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AdsKater.API.Controllers
{
    [RoutePrefix("api/Advertisements")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AddsController : ApiController
    {
        [HttpGet]
        [Route("GetAllAdvertisements")]
        public async Task<IHttpActionResult> GetAllAdvertisements()
        {
            try
            {
                using (AdsKaterEntities entity = new AdsKaterEntities())
                {
                    var addsData = entity.Advertisements.Where(p => p.IsActive);
                    if (addsData != null)
                    {
                        var advertisements = GetAdvertisements(addsData);

                        BaseResponse<IList<AdvertisementModel>> responce = new BaseResponse<IList<AdvertisementModel>>();
                        responce.Data = advertisements;
                        responce.Success = true;
                        return Ok(responce);
                    }
                    else
                    {
                        BaseResponse<string> responce = new BaseResponse<string>();
                        responce.Success = false;
                        responce.Message = "Sorry! There are no adds available";
                        return Ok(responce);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest(System.Configuration.ConfigurationManager.AppSettings["UnableToProcessRequest"]);
            }
        }

        [HttpGet]
        [Route("GetAdvertisementDetails")]
        public async Task<IHttpActionResult> GetAdvertisementDetails(long advertisementId)
        {
            try
            {
                using (AdsKaterEntities entity = new AdsKaterEntities())
                {
                    var addsData = entity.Advertisements.Where(p => p.IsActive && p.AdvertisementID == advertisementId);
                    if (addsData != null)
                    {
                        var advertisements = GetAdvertisements(addsData);

                        BaseResponse<AdvertisementModel> responce = new BaseResponse<AdvertisementModel>();
                        responce.Data = advertisements.FirstOrDefault();
                        responce.Success = true;
                        return Ok(responce);
                    }
                    else
                    {
                        BaseResponse<string> responce = new BaseResponse<string>();
                        responce.Success = false;
                        responce.Message = "Sorry! There are no adds available";
                        return Ok(responce);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return BadRequest(System.Configuration.ConfigurationManager.AppSettings["UnableToProcessRequest"]);
            }
        }

        private IList<AdvertisementModel> GetAdvertisements(IQueryable<Advertisement> addsData)
        {
            IList<AdvertisementModel> data = new List<AdvertisementModel>();
            foreach (var item in addsData)
            {
                AdvertisementModel add = new AdvertisementModel();
                add.AdvertisementID = item.AdvertisementID;
                add.Title = item.Title;
                add.Description = item.Description;
                add.VideoUrl = item.VideoUrl;
                add.PhotoUrl = item.PhotoUrl;
                add.StartDate = item.StartDate;
                add.EndDate = item.EndDate;
                add.ViewsCount = item.ViewsCount;
                IList<AdvertisementPrizeModel> prizes = new List<AdvertisementPrizeModel>();
                foreach (var prize in item.AdvertisementPrizes)
                {
                    AdvertisementPrizeModel advPrize = new AdvertisementPrizeModel();
                    advPrize.AdvertisementPrizesID = prize.AdvertisementPrizesID;
                    advPrize.PrizeTitle = prize.PrizeTitle;
                    advPrize.PrizeDescription = prize.PrizeDescription;
                    advPrize.PrizeLevel = prize.PrizeLevel;
                }

                add.Prizes = prizes;
                data.Add(add);
            }
            return data;
        }
    }
}