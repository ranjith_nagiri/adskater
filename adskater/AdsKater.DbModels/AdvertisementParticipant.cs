//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdsKater.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class AdvertisementParticipant
    {
        public long AdvertisementParticipantID { get; set; }
        public long AdvertisementsUsersID { get; set; }
        public System.DateTime ParticipantDate { get; set; }
    
        public virtual AdvertisementsUser AdvertisementsUser { get; set; }
    }
}
