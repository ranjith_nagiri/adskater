﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdsKater.DbModels;

namespace AdsKater.UserSite.Controllers
{
    [Authorize]
    public class AdvertisementPrizesController : Controller
    {
        private AdsKaterEntities db = new AdsKaterEntities();

        // GET: AdvertisementPrizes
        public ActionResult Index()
        {
            var advertisementPrizes = db.AdvertisementPrizes.Include(a => a.Advertisement);
            return View(advertisementPrizes.ToList());
        }

        // GET: AdvertisementPrizes/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdvertisementPrize advertisementPrize = db.AdvertisementPrizes.Find(id);
            if (advertisementPrize == null)
            {
                return HttpNotFound();
            }
            return View(advertisementPrize);
        }

        // GET: AdvertisementPrizes/Create
        public ActionResult Create(long? id)
        {
            if (id == null)
            {
                ViewBag.AdvertisementID = new SelectList(db.Advertisements, "AdvertisementID", "Title");
            }
            else
            {
                ViewBag.AdvertisementID = new SelectList(db.Advertisements.Where(a => a.AdvertisementID == id), "AdvertisementID", "Title");
            }
            return View();
        }

        // POST: AdvertisementPrizes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AdvertisementPrizesID,AdvertisementID,PrizeTitle,PrizeDescription,PrizeLevel")] AdvertisementPrize advertisementPrize)
        {
            if (ModelState.IsValid)
            {
                db.AdvertisementPrizes.Add(advertisementPrize);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AdvertisementID = new SelectList(db.Advertisements, "AdvertisementID", "Title", advertisementPrize.AdvertisementID);
            return View(advertisementPrize);
        }

        // GET: AdvertisementPrizes/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdvertisementPrize advertisementPrize = db.AdvertisementPrizes.Find(id);
            if (advertisementPrize == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdvertisementID = new SelectList(db.Advertisements, "AdvertisementID", "Title", advertisementPrize.AdvertisementID);
            return View(advertisementPrize);
        }

        // POST: AdvertisementPrizes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AdvertisementPrizesID,AdvertisementID,PrizeTitle,PrizeDescription,PrizeLevel")] AdvertisementPrize advertisementPrize)
        {
            if (ModelState.IsValid)
            {
                db.Entry(advertisementPrize).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AdvertisementID = new SelectList(db.Advertisements, "AdvertisementID", "Title", advertisementPrize.AdvertisementID);
            return View(advertisementPrize);
        }

        // GET: AdvertisementPrizes/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdvertisementPrize advertisementPrize = db.AdvertisementPrizes.Find(id);
            if (advertisementPrize == null)
            {
                return HttpNotFound();
            }
            return View(advertisementPrize);
        }

        // POST: AdvertisementPrizes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            AdvertisementPrize advertisementPrize = db.AdvertisementPrizes.Find(id);
            db.AdvertisementPrizes.Remove(advertisementPrize);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}