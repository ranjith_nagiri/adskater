﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdsKater.DbModels;

namespace AdsKater.UserSite.Controllers
{
    [Authorize]
    public class AdvertisementsController : Controller
    {
        private AdsKaterEntities db = new AdsKaterEntities();

        // GET: Advertisements
        public ActionResult Index()
        {
            return View(db.Advertisements.ToList());
        }

        // GET: Advertisements/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisements.Find(id);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            return View(advertisement);
        }

        // GET: Advertisements/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Advertisements/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AdvertisementID,Title,Description,VideoUrl,PhotoUrl,StartDate,EndDate,ViewsCount,IsActive")] Advertisement advertisement, IEnumerable<HttpPostedFileBase> files)
        {
            if (ModelState.IsValid)
            {
                advertisement.ViewsCount = 0;
                advertisement.PhotoUrl = string.Empty;
                db.Advertisements.Add(advertisement);
                db.SaveChanges();

                foreach (var file in files)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        try
                        {
                            string fileName = advertisement.AdvertisementID + "_" + file.FileName;
                            file.SaveAs(Path.Combine(Server.MapPath("/AdvertisementImages"), fileName));
                            advertisement.PhotoUrl = fileName;
                            db.SaveChanges();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }
                        catch (DbUpdateException updExc)
                        {
                            string errorMessage = updExc.InnerException.Message;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
                return RedirectToAction("Index");
            }

            return View(advertisement);
        }

        // GET: Advertisements/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisements.Find(id);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            return View(advertisement);
        }

        // POST: Advertisements/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AdvertisementID,Title,Description,VideoUrl,PhotoUrl,StartDate,EndDate,ViewsCount,IsActive")] Advertisement advertisement, IEnumerable<HttpPostedFileBase> files)
        {
            if (ModelState.IsValid)
            {
                db.Entry(advertisement).State = EntityState.Modified;
                db.SaveChanges();
                foreach (var file in files)
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        try
                        {
                            string fileName = advertisement.AdvertisementID + "_" + file.FileName;
                            file.SaveAs(Path.Combine(Server.MapPath("/AdvertisementImages"), fileName));
                            advertisement.PhotoUrl = fileName;
                            db.SaveChanges();
                        }
                        catch (DbEntityValidationException e)
                        {
                            foreach (var eve in e.EntityValidationErrors)
                            {
                                Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage);
                                }
                            }
                            throw;
                        }
                        catch (DbUpdateException updExc)
                        {
                            string errorMessage = updExc.InnerException.Message;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
                return RedirectToAction("Index");
            }
            return View(advertisement);
        }

        public ActionResult AddPrizes(long? id)
        {
            return RedirectToAction("Create", "AdvertisementPrizes", new { id = id });
        }

        // GET: Advertisements/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisements.Find(id);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            return View(advertisement);
        }

        // POST: Advertisements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Advertisement advertisement = db.Advertisements.Find(id);
            db.Advertisements.Remove(advertisement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}