﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AdsKater.UserSite.Startup))]
namespace AdsKater.UserSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
