﻿namespace AdsKater.Models
{
    public class AdvertisementPrizeModel
    {
        public long AdvertisementPrizesID { get; set; }
        public string PrizeTitle { get; set; }
        public string PrizeDescription { get; set; }
        public int PrizeLevel { get; set; }
    }
}