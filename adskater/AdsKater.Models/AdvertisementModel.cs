﻿using System.Collections.Generic;

namespace AdsKater.Models
{
    public class AdvertisementModel
    {
        public long AdvertisementID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string VideoUrl { get; set; }
        public string PhotoUrl { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public long ViewsCount { get; set; }
        public IList<AdvertisementPrizeModel> Prizes { get; set; }
    }
}