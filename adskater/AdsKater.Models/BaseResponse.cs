﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdsKater.Models
{
    public class BaseResponse<T>
    {
        public T Data { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}